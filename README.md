# PySL
Pythonic SDN Lab

*This repository is still under development and functionality may be added or removed based on needs*

This repository contains the tool kit I am building to experiment with software defined network and appliancs as well as LXD containers. This work is by no way complete and I plan on continuing to implement features that support my needs.

## Lab
Defined lab archeticture can be found in configs/lab.yaml

### Built with
- Ubuntu Server 18.04
- Python 3.7
- OpenVSwitch

### Getting Started
``` git clone gitlab.com/trvon/PySL ```

``` pip install -r requirements.txt ```

### Usage
Below are arguemnts and the actions these arguements perform on the machines LXD state

```python main [args]```

Arguments:
- refresh: Updates the fingerprints in the .yaml file from the linuxcontainers.org database
- destroy: Tears down the lab [this includes OVS and LXD states]
- init: Spawn the machines and network configuration detailed in lab.yaml
- provision: Sets a static IP for all containers and begins installation process
- IP: Sets a static IP for all containers based on .yaml configuration
