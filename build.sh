#!/bin/bash

if  [ `whoami` == root ]; then
	echo "Do not run as root!"
	exit
fi

# Ubuntu install 
sudo apt install openvswitch-switch openvswitch-common lxd lxd-tools python3 python3-pip

## LXD
# lxd init

# Python
sudo pip3 install -r requirements.txt

# Openvswitch
sudo ovs-vsctl set-manager ptcp:6640 #:$IP_REMOTE
# ovsdb-server --remote=ptcp:6640

# Ansible
## Add check to see if config is already there
echo -e "[defaults]\nhost_key_checking = False" >> ~/.ansible.cfg
