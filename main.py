#!/usr/bin/python3
# Author: Trevon Williams
# Purpose: To streamline the creation of my lab for my Masters thesis

import getpass
import sys
import os
# Documentation: https://buildmedia.readthedocs.org/media/pdf/python-ovs-vsctl/latest/python-ovs-vsctl.pdf
from modules import Api, Lab, Ovs, Images, Dialog
from modules.machines import Initialize

# Global Variables for Lab
file = 'configs/lab.yaml'

# TODO: Update YAML File fingerprints
def refresh():
    Images.getFingerprints()

## Read YAML file container network architecture
def init():
    username = input("Username [%s]: " % getpass.getuser())
    if not username:
        username = getpass.getuser()
    myLab = Lab.parseLab('configs/lab.yaml')
    Ovs.init()
    Lab.createLab(myLab)
    # Updates Containers
    # TODO: Rename function and give it a more meaningful name
    Initialize.init(username)

def provision():
    myLab = Lab.parseLab(file)
    lab = [ i for i in myLab ]
    # Assign Container IP's
    Lab.assignIp(myLab)
    Initialize.salt(lab)
    # Start controller
    Lab.startController(myLab)

def destroy():
    myLab = Lab.parseLab('configs/lab.yaml')
    Lab.deleteLab(myLab)

## Create containers on openvswitch switch
if sys.argv[1] == 'init':
    init()
elif sys.argv[1] == 'provision':
    provision()
elif sys.argv[1] == 'destroy':
    destroy() 
elif sys.argv[1] == 'refresh':
    refresh() 
elif sys.argv[1] == 'ip':
    myLab = Lab.parseLab('configs/lab.yaml')
    Lab.assignIp(myLab)
elif sys.argv[1] == 'help':
    Dialog.help()
elif sys.argv[1] == 'clean':
    Images.cleanYaml()


# Configuration of containers (ssh certs, installation of ssh, adding users, allow user to use root with password)



## Start API for automating how the contoller interacts with each container


