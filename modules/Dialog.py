#!/usr/bin/python

def help():
    print("\npython [args]"
          "\n\nArguments are included and described below" + 
          "\n\tip\t\t-- sets static IP's for containers based on .yaml file" +
          "\n\tinit\t\t-- spawns machines from lab.yaml file" +
          "\n\tclean\t\t-- removes added fingerpints in lab.yaml file" +
          "\n\trefresh\t\t-- Updates linuxcontainers.org fingerprints" +
          "\n\tdestroy\t\t-- Tears down lab" +
          "\n\tprovision\t-- sets static IP's for containers and starts install processes")
