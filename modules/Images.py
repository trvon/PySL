#!/bin/bash
import re
import json
import yaml
import requests
import multiprocessing

from modules import Lab

def getFingerprints():
    r = requests.get('https://us.images.linuxcontainers.org/1.0/images')
    fingerprints = r.text.replace('/1.0/images/', '').strip('\n').split(', ')[4:-1] # This will skip the first fingerprint
    fingerprints = [i for i in fingerprints if i is not None ] 
    p = multiprocessing.Pool(4)
    print("Starting Check")
    p.map(pullInfo, fingerprints)
    
def pullInfo(images):
    lab = Lab.parseLab('configs/lab.yaml')
    url = 'https://us.images.linuxcontainers.org/1.0/images/' + images.strip('"')
    r = requests.get(url)
    response = ""
    # Fixing NONE Type returning when page doesn't return anything
    if r is None:
        return
    try:
        response = json.loads(r.text)
    except Exception as e:
        print("ERROR RAISED: %s, %s" % (r.status_code, e))
        return

    info = response['metadata']['properties']['description']
    info = ' '.join(re.findall('\[[^\]]*\]|\([^\)]*\)|\"[^\"]*\"|\S+',info)[:-1])
    status = check(images.strip('"'), lab, info)
    name = status[1]
    status = status[0]
    # If fingerprint exists and is not updated then update
    if status:
        updateYaml(images.strip('"'), name)

def check(fingerprint, lab, info):
    try:
        for machines in lab:
            if lab[machines]['OS'].lower() in info.lower():
                current_fingerprint = lab[machines]["source"]["alias"]
                if current_fingerprint is None:
                    return (True, machines)

                # Checks if old fingerprint is being used
                r = requests.get('https://us.images.linuxcontainers.org/1.0/images/' + current_fingerprint)
                if r.status_code == '404':
                    return (True, machines)

    except Exception as e:
        # TODO: Fix Nonetype error
        print("ERROR RAISED: %s" % e)

    return (False, None)

def updateYaml(fingerprint, name):
    print("Updating ", name, " with fingerprint: ", fingerprint)
    updates = Lab.parseLab('configs/lab.yaml')
    updates[name]['source']['alias'] = fingerprint

    # TODO: Fix Me
    # Should introduce race condition unless multiprocessing handles this
    with open('configs/lab.yaml', 'w') as f:
        f.write(yaml.dump(updates, default_flow_style=False))
    f.close()

# Edits the yaml file and removes the fingerprints
def cleanYaml():
    with open("configs/lab.yaml") as f:
        lab_doc = yaml.safe_load(f)

    for machine in lab_doc:
        if lab_doc[machine]["source"]["alias"] is not None:
            lab_doc[machine]["source"]["alias"] = None

    with open("configs/lab.yaml", "w") as f:
        yaml.dump(lab_doc, f)
