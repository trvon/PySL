#!/usr/bin/python
import multiprocessing
import yaml
import time
import sys
# Documentation: https://pylxd.readthedocs.io/en/latest/
import pylxd

# Custom libraries
from modules import machines
from modules import Ovs

# Parses lab.yaml file
def parseLab(file):
    with open(file, 'r') as f:
        try:
            labmap = yaml.safe_load(f)
        except yaml.YAMLError as e:
            print(e)
    return labmap

# Number Vlans
def createOctets(lab):
    try:
        network = list(set(["ovs-" + lab[i]["network"] for i in lab] )) # VLANS Spaces 
        network = sorted(network, reverse=True)
        network = [ (i, x) for x, i in enumerate(network)] # VLAN Numbering
    except Exception as e:
        print("ERROR RAISED: Your .yaml file may be wrong")
        print(e)
    return network

# Creates containers and connects them to network profiles
def createLab(lab):
    p = multiprocessing.Pool(4) # By default use 4 workers
    network = createOctets(lab)
    machines = [(i, lab[i]) for i in lab]
    p.map(createNetworks, network)
    p.map(createMachine, machines)
    # TODO: Remove the need for another command to be ran
    # print("Run `python main.py provision` when IP's are loaded or machines are connected to network")

# Creates individual machines
def createMachine(info):
    client = pylxd.Client()
    profile = "ovs-" + info[1]["network"]
    config = {'name': info[0], 'source': info[1]['source'], 'profiles': [profile]}
    try:
        print("Creating %s" % info[0])
        container = client.containers.create(config, wait=True)
        container.start(wait=False)
    except Exception as e:
        print(e)

# TODO: Change how controller name is determined
def startController(lab):
    client = pylxd.Client()
    controllers = [ i for i in lab if "controller" in i]
    for controller in controllers:
        client.containers.get(controller).execute("git clone http://github.com/noxrepo/pox && cd pox".split())
        client.containers.get(controller).execute("./pox.py --no-cli forwarding.l2_learning openflow.spanning_tree --no-flood host_tracker".split())
        Ovs.startController(controller)

# Starts assigning the machine IP
def assignIp(lab):
    machine_info = [(i, lab[i]) for i in lab]
    p = multiprocessing.Pool(4) # By default use 4 workers
    try:
        p.map(setIp, machine_info)
    except Exception as e:
        print("ERROR %s" % e)

# Sets the specific IP for the machine
## Helper function for AssignIP
def setIp(info): 
    client = pylxd.Client()
    print("Setting ip of %s" % info[0])
    # Had to add local variables because Pools do not have shared memory
    # TODO: When adding VLAN's, know that the names are sorted in reverse
    network = ['prod', 'dmz', 'client'] # TODO: When adding more VLAN names, add them here
    octet = str(network.index(info[1]['network']) * 10)
    gateway = "10.0.x.1".replace("x", octet)
    lan = "10.0.x.0/24".replace("x", octet)
    # Getting old container ip
    ip = info[1]['ip'].replace("x", octet)
    ip = str("ip addr add %s/24 dev eth0" % ip).split()
    # Replaces the IP address
    client.containers.get(info[0]).execute("ip addr flush dev eth0".split(' '))
    client.containers.get(info[0]).execute(ip)
    client.containers.get(info[0]).execute(str("ip route add %s dev eth0" % lan).split(' '))
    client.containers.get(info[0]).execute(str("ip route add 0.0.0.0/0 via %s" % gateway).split(' '))
        

# Creates specified network adapters
def createNetworks(vlan):
    # TODO: Fix nework Addressing
    # PROD: 0, DMZ: 10, CLIENT: 20
    client = pylxd.Client()
    try:
        ip = "10.0.x.1/24".replace("x", str(vlan[1] * 10))
        config = {"bridge.driver": "openvswitch", "ipv4.address": ip, "ipv4.nat": "true", "ipv6.nat": "false"}
        name = vlan[0]
        client.networks.create(name, description="%s's network" % name, type="bridge", config=config)
        createProfile(name)
        Ovs.updateBridges(name)
        Ovs.addPort(name)
        print(name, "created")
    except pylxd.exceptions.LXDAPIException:
        print("Network already created")

# Creates LXD containers profile
def createProfile(vlan_name):
    client = pylxd.Client()
    devices = {"eth0": {"nictype": "bridged", "parent": vlan_name, "type": "nic"}, 
                        "root": { "path": "/", "pool": "default", "type": "disk"}}
    config={'security.nesting': 'true'}
    client.profiles.create(vlan_name,config=config, devices=devices)

# Deletes Lab infrastructure
def deleteLab(lab):
    networks = set(lab[i]["network"] for i in lab)
    for name in lab:
        deleteContainers(name)

    for network in networks:
        deleteProfile(network)
        deleteNetworks(network)
        Ovs.clean(network)

# Deletes network adapters
def deleteNetworks(vlan):
    client = pylxd.Client()
    try:
        name = ''.join(('ovs-', vlan))
        client.networks.get(name).delete()
    except pylxd.exceptions.NotFound:
        print("Network already deleted")

# Deletes Container Network
def deleteProfile(vlan):
    client = pylxd.Client()
    try:
        name = "ovs-" + vlan
        client.profiles.get(name).delete()
    except pylxd.exceptions.NotFound:
        print("Profile already deleted")

# Deletes Container
def deleteContainers(name):
    client = pylxd.Client()
    try:
        client.containers.get(name).stop(wait=True)
        client.containers.get(name).delete(wait=True)
    except Exception as e:
        print("Container does not exist")
