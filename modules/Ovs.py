#!/usr/bin/python

import os
# Imported libraries
from modules import Lab

import ovs_vsctl
import pylxd

# Intializing OVS manager
def init():
    vsctl = ovs_vsctl.VSCtl('tcp', '127.0.0.1',port=6640)
    vsctl.run(command="set-manager ptcp:6640")

# Adds port to ovs-bridges
# TODO: Add a dependency check and creation check 
def updateBridges(adapter):
    vsctl = ovs_vsctl.VSCtl('tcp', '127.0.0.1',port=6640)
    adapter = adapter.replace("ovs-", "")
    # print("Creating Bridge %s" % adapter)
    # os.system("sudo brctl addbr MGMT-%s && sudo ip link set MGMT-%s up" % (adapter, adapter))
    # vsctl.run(command="add-br MGMT-$s", % adapter)


# Adding the ports to the bridges
def addPort(adapter):
    interface = adapter.replace("ovs-", "")
    vsctl = ovs_vsctl.VSCtl('tcp', '127.0.0.1',port=6640)
    # command = "add-port %s MGMT-%s" % (adapter, interface)
    # vsctl.run(command=command)

# Start controller
def startController(controller):
    client = pylxd.Client()
    vsctl = ovs_vsctl.VSCtl('tcp', '127.0.0.1',port=6640)
    ip = client.containers.get(controller).state().network['eth0']['addresses'][0]['address']
    bridges = ['ovs-prod', 'ovs-dmz', 'ovs-client']
    # TODO: Change setting controller on bridges in the future
    for bridge in bridges:
        vsctl.run(command="set-controller %s tcp:%s:6633" % (bridge, ip)) 

def clean(adapter):
    adapter = adapter.replace("ovs-", "")
    vsctl = ovs_vsctl.VSCtl('tcp', '127.0.0.1',port=6640)
    # os.system("sudo ip link set MGMT-%s down" % adapter)
    # try:
        # os.system("sudo brctl delbr MGMT-%s" % adapter)
    # except Exception as e:
    #    print("Bridge not Created")
