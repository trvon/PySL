#!/usr/bin/python

import multiprocessing
import time

import pylxd

from IPy import IP
from modules import Lab

# Initialization of Multiprocessing Function
def init(username):
    p = multiprocessing.Pool(4)
    lab = Lab.parseLab('configs/lab.yaml')
    info = [(lab[i]['package-manager'], i) for i in lab]
    # TODO: Control is returning errors
    try:
        p.map(config, info)
    except Exception as e:
        print("ERROR %s : Continuing..." % e)

def salt(machines):
    client = pylxd.Client()
    p = multiprocessing.Pool(4)
    try:
        p.map(saltConfig, machines)
    # TODO: Control is returning errors
    except Exception as e:
        print("ERROR %s" % e)
    # Accept Keys on Pox-controller
    while len(client.containers.get('pox-controller').execute("salt-key -l unaccepted".split(' '))[1].split('\n')) < 2:
        print('No Keys')
        time.sleep(1)
    print("Accepting SaltStack Keys...")
    client.containers.get('pox-controller').execute("salt-key -A -y".split(' '))


# SaltStack Specific Configuration
def saltConfig(machine):
    client = pylxd.Client()
    if machine == "pox-controller":
        client.containers.get(machine).execute('systemctl restart salt-master'.split(' '))
    else:
        with open('configs/minion') as minion:
            client.containers.get(machine).files.put('/etc/salt/minion', minion)
        client.containers.get(machine).execute('systemctl restart salt-minion'.split(' '))

# Updates Containers
def config(info):
    machine = info[1]
    packageManager = info[0]
    client = pylxd.Client()
    # Wait until machine connections to internet
    while IP(client.containers.get(machine).state().network['eth0']['addresses'][0]['address']).version() != 4:
        time.sleep(1)

    # TODO: Change to dynamically get the VLAN Controller IP
    # Debian Based
    if (packageManager == 'apt'):
        print("Updating %s" % machine)
        client.containers.get(machine).execute("wget -O - https://repo.saltstack.com/py3/ubuntu/18.04/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -".split(' '))
        client.containers.get(machine).execute("echo 'deb http://repo.saltstack.com/py3/ubuntu/18.04/amd64/latest bionic main' > /etc/apt/source.list.d/saltstack.list".split(' '))
        client.containers.get(machine).execute("apt-get update".split(' '))
        # client.containers.get(machine).execute("apt-get upgrade -y".split(' '))
        client.containers.get(machine).execute("apt-get install sudo git salt-minion -y".split(' '))
        client.containers.get(machine).execute("systemctl start salt-minion".split(' '))
        client.containers.get(machine).execute("systemctl enable salt-minion".split(' '))
    # RHEL Based
    elif (packageManager == 'yum'):
        print("Updating %s, this may take some time" % machine)
        client.containers.get(machine).execute("yum install http://repo.saltstack.com/yum/redhat/salt-repo-latest.el7.noarch.rpm -y".split(' '))
        client.containers.get(machine).execute("yum clean expire-cache".split(' '))
        # client.containers.get(machine).execute("dnf update -y".split(' '))
        if machine == "pox-controller":
            client.containers.get(machine).execute("yum install sudo git salt-master -y".split(' '))
            client.containers.get(machine).execute("systemctl enable salt-master".split(' '))
        else:
            client.containers.get(machine).execute("yum install sudo git salt-minion -y".split(' '))
            client.containers.get(machine).execute("systemctl start salt-minion".split(' '))
            client.containers.get(machine).execute("systemctl enable salt-minion".split(' '))
        client.containers.get(machine).execute("ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa".split(' '))
    # Archlinux
    elif (packageManager == 'pacman'):
            client.containers.get(machine).execute("pacman -Sy git salt python3 --noconfirm".split(' '))
    # Alpine
    elif (packageManager == 'apk'):
            # TODO: Fix, this is kinda bad
            client.containers.get(machine).execute("wget -O https://bootstratp.saltstack.com/develop | sh -s -- -U -M -X -d -D".split(' '))
            client.containers.get(machine).execute("apk add docker".split(' '))
            client.containers.get(machine).execute("rc-update add docker boot".split(' '))
            client.containers.get(machine).execute("service docker start".split(' '))
    else:
        print("%s's configured packagemanager is not currently supported")
